from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import NamaKegiatan, DaftarKegiatan
from .models import CreateModel, RegisterModel


def home(request):
	posts 	= CreateModel.objects.all()
	post 	= RegisterModel.objects.all()

	context ={
		'page_title': 'Daftar Kegiatan',
		'posts_form' : posts,
		'post_form' : post,
	}
	return render(request, 'main/home.html', context)

def create(request):
	posts_form = NamaKegiatan(request.POST or None)
	if request.method == "POST":
		if posts_form.is_valid():
			posts_form.save()
			
			return redirect ("main:home")
	
	context = {
		'page_title': 'Event',
		'posts_form' : posts_form,
	}
	return render (request, 'main/create.html', context)

def register(request):
	post_form = DaftarKegiatan(request.POST or None)
	if request.method == "POST":
		if post_form.is_valid():
			people = post_form.save(commit=False)
			people.kegiatan_id = CreateModel.objects.get(id=index).id
			people.save()

			return redirect("main:home")
		
	context = {
		'page_title': 'Register',
		'post_form' : post_form,
	}
	return render (request, 'main/register.html', context)
