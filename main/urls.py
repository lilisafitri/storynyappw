from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('create/', views.create, name='create'),
    path('register<int:index/', views.register, name='register')
    

]
