from django.db import models

# Create your models here.
class CreateModel (models.Model):
	event 		= models.CharField(max_length=50)

	def __str__(self):
		return "{}".format(self.event)

class RegisterModel(models.Model):
	kegiatan = models.ForeignKey(CreateModel, on_delete = models.CASCADE)
	register = models.CharField(max_length=50)

	def __str__(self):
		return "{}".format(self.register)

