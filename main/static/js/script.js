//event pada saat link di klik
$('.nav-link').on('click', function(e) {
    
    //ambil isi href
    var tujuan = $(this).attr('href');
    //tangkap elemen ybs
    var elemenTujuan = $(tujuan);

    //pindahkan scroll
    $('body').animate({
        scrollTop: elemenTujuan.offset().top-70
    }, 1250, 'easeInOutExpo')

    e.preverentDefault();
});