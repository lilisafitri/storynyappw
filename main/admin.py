from django.contrib import admin

# Register your models here.

from .models import CreateModel, RegisterModel
		
admin.site.register(CreateModel)
admin.site.register(RegisterModel)