from django import forms
from .models import CreateModel, RegisterModel

class NamaKegiatan(forms.ModelForm):
	class Meta:
		model = CreateModel
		fields = [
		'event'
		]

		labels = {
			'event' : 'Nama Kegiatan',
		}

		widgets = {
			'event' : forms.TextInput(
				attrs = {
					'class' : 'form-control',
					'placeholder' : 'Masukkan nama event',}
				)
		}

class DaftarKegiatan(forms.ModelForm):
	class Meta:
		model = RegisterModel
		fields = [
		'register'
		]

		labels = {
			'register' : 'Nama Lengkap',
		}

		widgets = {
			'register' : forms.TextInput(
				attrs = {
					'class' : 'form-control',
					'placeholder' : 'Masukkan nama Anda',}
				)
		}


 